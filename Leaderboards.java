import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Leaderboards extends Actor {

	public Leaderboards() {
		GreenfootImage newImage = new GreenfootImage(200, 260);
		setImage(newImage);

	}
	//Define a posição e as características do placar Top 10
	public void setLeaderboard(String leaderboard) {
		GreenfootImage newImage = getImage();
		newImage.clear();

		Color c = new Color(255, 194, 0, 255);
		Font f = new Font("Arial Bold", 18);
		newImage.setFont(f);

		newImage.setColor(c);
		newImage.fill();
		newImage.setColor(Color.WHITE);
		newImage.drawString(leaderboard, 10, 25);
		setImage(newImage);

	}

}
