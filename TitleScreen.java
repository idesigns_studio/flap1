import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
//Tela inicial do jogo
public class TitleScreen extends World {

	public TitleScreen() {

		super(800, 600, 1, false);

		// Logo
		Logo logo = new Logo();
		addObject(logo, 400, 260);
		Start start = new Start();
		addObject(start, 400, 400);

	}

	public void act() {

	}
}
