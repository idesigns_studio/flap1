import java.io.*;

public class Ranking {
	private static final String FILE = "ranking.txt";
	private static Player[] players;

	public static Player[] add(Player player) {
		// se jogador n�o for top 10, retorna a lista sem ele dos top 10

		if (!isTop10(player))
			return players;

		int i;

		// adiciona jogador temporariamente
		Player[] tempPlayers = new Player[11];
		for (i = 0; i < players.length; i++)
			tempPlayers[i] = players[i];
		tempPlayers[tempPlayers.length - 1] = player;

		// ordena a lista
		i = 10;
		while (i > 0 && (tempPlayers[i - 1] == null || tempPlayers[i].getScore() >= tempPlayers[i - 1].getScore())) {
			Player temp = tempPlayers[i];
			tempPlayers[i] = tempPlayers[i - 1];
			tempPlayers[i - 1] = temp;
			i--;
		}
		for (Player p : tempPlayers) {
			if (p != null)
				System.out.println(p.getName() + "=" + p.getScore());
			else
				System.out.println("null");
		}
		for (i = 0; i < 10; i++)
			players[i] = tempPlayers[i];
		return getClean();

	}
	// Carrega vetor dos top 10 jogadores
	public static Player[] load() {
		try {
			BufferedReader file = new BufferedReader(new FileReader(new File(FILE)));
			String line = "", values[];

			players = new Player[10];
			int i = 0;
			while ((line = file.readLine()) != null) {
				values = line.split(";");
				players[i++] = new Player(values[0], Integer.parseInt(values[1]));
			}
			file.close();
			return getClean();
		} catch (IOException e) {
			e.printStackTrace();
			return players = new Player[10];
		}
	}
	// Salva em arquivo os top 10 jogadores
	public static void save() {
		try {
			BufferedWriter file = new BufferedWriter(new FileWriter(new File(FILE)));
			for (int i = 0; i < players.length && players[i] != null; i++) {
				file.write(players[i].getName() + ";" + players[i].getScore());
				file.newLine();
			}
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//Checa se o score � suficiente para entrar no ranking top 10
	public static boolean isTop10(Player player) {
		for (Player p : players)
			if (p == null || p.getScore() < player.getScore())
				return true;
		return false;
	}

	// retorna uma lista de jogadores sem os null
	public static Player[] getClean() {
		int size;
		for (size = 0; size < players.length; size++) {
			if (players[size] == null) {
				Player[] tempPlayers = new Player[size];
				for (int i = 0; i < size; i++)
					tempPlayers[i] = players[i];
				return tempPlayers;
			}
		}
		return players;
	}
}
