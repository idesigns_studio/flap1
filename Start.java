import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
// Bot�o Start
public class Start extends Actor {

	public void act() {
		if (Greenfoot.mouseClicked(this)) {
			FlappyBird.gameOver.stop();
			FlappyBird.resetVariables();
			Greenfoot.setWorld(new FlappyWorld());
		}

	}
}
