// (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.*;
import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.GreenfootSound;

public class FlappyBird extends Actor {

	double yPosition = 0;
	double gravity = .5;
	double boost_speed = -8;
	int score = 0;
	String name = "";

	//Sons do Jogo
	GreenfootSound jump = new GreenfootSound("jump.mp3");
	GreenfootSound point = new GreenfootSound("point.mp3");
	GreenfootSound congratulations = new GreenfootSound("top10.mp3");
	static GreenfootSound gameOver = new GreenfootSound("game_over.mp3");

	public void act() {

		jump();
		updateScore();
		gameOverConditions();

	}

	// Ao pressionar o mouse, personagem "pula"
	private void jump() {

		if (FlappyWorld.pipeSpeed != 0) {
			if (Greenfoot.mousePressed(null) == true) {
				yPosition = boost_speed;
				jump.play();
			}

			setLocation(getX(), (int) (getY() + yPosition));

			// Determina �ngulo do personagem
			if (yPosition > -10 && yPosition < 0) {
				setRotation(-30);
			} else if (yPosition > 0 && yPosition < 6) {

				setRotation(0);

			} else if (yPosition > 6) {
				setRotation(30);
			}
			yPosition = yPosition + gravity;
		}
	}

	// Se tocar no marcador de pontos ++
	private void updateScore() {
		if (getOneIntersectingObject(ScoreMarker.class) != null) {
			score++;
			FlappyWorld.score = score;
			removeTouching(ScoreMarker.class);
			point.play();

		}
	}

	private void gameOverConditions() {

		// Se tocar no cano ou cair do mundo, GAME OVER
		if (getOneIntersectingObject(Pipe.class) != null
				|| getOneIntersectingObject(TopPipe.class) != null
				|| getY() > getWorld().getHeight()) {
			gameOver();

		}

	}

	// M�todo para exibir o fim do jogo
	private void gameOver() {
		Greenfoot.stop();

		if (name == "") {
			Player player = new Player(name, score);
			GameOver over = new GameOver();

			getWorld().addObject(over, getWorld().getWidth() / 2, getWorld().getHeight() / 2);
			Start start = new Start();
			getWorld().addObject(start, 400, 400);

			//Se Score estiver entre os Top 10, pegar nome do usu�rio
			if (Ranking.isTop10(player)) {
				congratulations.play();
				name = JOptionPane.showInputDialog("Digite seu nome:");
				player = new Player(name, score);
				Ranking.add(player);
				Ranking.save();
			} else {
				FlappyWorld.backgroundMusic.stop();
				gameOver.play();
			}

		}
		
		//Parar o movimento mundo
		FlappyWorld.pipeSpeed = 0;
		FlappyWorld.pipeDistance = 0;
		Greenfoot.start();

	}

	// M�todo para resetar velocidade e pontua��o
	public static void resetVariables() {
		FlappyWorld.score = 0;
		FlappyWorld.pipeSpeed = -4;
		FlappyWorld.pipeDistance = 80;

	}

}
