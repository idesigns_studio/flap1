// (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import greenfoot.Actor;
import greenfoot.Color;
import greenfoot.Font;
import greenfoot.GreenfootImage;

public class Score extends Actor {

	public Score() {
		GreenfootImage newImage = new GreenfootImage(100, 60);
		setImage(newImage);

	}
	//Define a posi��o e as caracter�sticas do placar do jogo em andamento
	public void setScore(int score) {
		GreenfootImage newImage = getImage();
		newImage.clear();

		Color c = new Color(255, 194, 0, 255);
		Font f = new Font("Impact", 24);
		Font n = new Font("Impact", 24);
		newImage.setFont(f);

		newImage.setColor(c);
		newImage.fill();
		newImage.setColor(Color.WHITE);
		newImage.drawString("Score",10,30);
		newImage.drawString(""+ score, 10, 53);
		setImage(newImage);

	}

}
