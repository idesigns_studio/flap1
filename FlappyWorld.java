// (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;
import greenfoot.GreenfootSound;
import greenfoot.World;

public class FlappyWorld extends World {
	int pipeCounter = 0;
	int space_pipes = 300;
	Score scoreObj = null;
	Leaderboards leaderboardObj = null;
	static int score = 0;
	static int pipeSpeed = -4;
	static int pipeDistance = 80;
	// boolean listen = false;
	
	// som do jogo
	static GreenfootSound backgroundMusic = new GreenfootSound("music.mp3");

	public FlappyWorld() {
		// Criar mundo no tamanho 800x600 células, onde cada celula é igual a 1px
		super(800, 600, 1, false);
		backgroundMusic.playLoop();
		
		//Coloca o ranking em memória
		Ranking.load();
		
		// Ordem dos objetos no mundo
		setOrder();

		// Instanciar o Flappy
		FlappyBird flappy = new FlappyBird();

		// Inserir o Flappy no mundo no meio da tela
		addObject(flappy, getWidth() / 2, getHeight() / 2);

		// Criar novo objeto de pontuação e dá o valor de 0
		scoreObj = new Score();
		scoreObj.setScore(0);

		// Inseri pontuação no mundo na posição X,Y
		addObject(scoreObj, 60, 40);

		// Criar objeto Placar
		leaderboardObj = new Leaderboards();
		leaderboardObj.setLeaderboard(getPlayers());

		// Inseri pontuação no mundo na posição X,Y
		addObject(leaderboardObj, 110, 210);

		// Inserir chão
		createGround();

	}

	// Onde os inputs são "escutados"
	public void act() {

		// Inserir Canos
		pipeCounter++;
		createPipes();

		// Mudar velocidade
		speedBoost();

		// Listener - Aumentar Pontuação
		scoreObj.setScore(score);
	}

	//Escreve no Placar
	public static String getPlayers() {
		String aux = "TOP 10 \n";

		Player[] players = Ranking.getClean();
		for (Player p : players) {
			aux += p.getName() + " " + p.getScore() + " \n";
		}

		return aux;
	}

	// Método para gerar os canos e o marcardor de pontos
	private void createPipes() {

		Pipe pipe = new Pipe();
		TopPipe topPipe = new TopPipe();
		ScoreMarker scoreMarker = new ScoreMarker();

		int randomNumber = Greenfoot.getRandomNumber(100) * 2;

		GreenfootImage image = pipe.getImage();

		if (pipeDistance != 0) {
			if (pipeCounter % pipeDistance == 0) {
				addObject(pipe, getWidth(), getHeight() / 2 + image.getHeight() / 2 + randomNumber);
				addObject(topPipe, getWidth(), image.getHeight() / 2 + randomNumber - space_pipes);
				addObject(scoreMarker, getWidth() + 100, getHeight() / 2);

			}
		}

	}

	private void createGround() {
		Ground ground = new Ground();
		addObject(ground, getWidth() / 2, getHeight() - 30);

	}
	
	private void setOrder(){
		setPaintOrder(
				Start.class,
				GameOver.class,
				FlappyBird.class,
				Score.class,
				Leaderboards.class,
				Ground.class,
				ScoreMarker.class,
				Pipe.class,
				TopPipe.class,
				FlappyWorld.class);

	}

	// TODO mostrar imagem que vai acelerar antes de acelerar
	/*
	 * private void showSpeedBoost(){ if(listen == false){
	 * 
	 * Ground ground = new Ground(); SpeedUp speedBoost = new SpeedUp();
	 * addObject(speedBoost,getWidth(),getHeight()/2 -150);
	 * 
	 * listen = true; } }
	 */
	
	//TODO criar método para reutilizar canos que saem da tela ou destruir os mesmos.

	private void speedBoost() {

		// TODO desenvolver uma forma menos gambiarra de fazer esses passos
		if (score != 0) {
			if (score == 10) {
				pipeSpeed = -5;
				pipeDistance = 60;

			} else if (score == 20) {
				pipeSpeed = -7;

			} else if (score == 30) {
				pipeSpeed = -9;
				pipeDistance = 50;
			} else if (score == 50) {
				pipeSpeed = -11;
				pipeDistance = 50;
			}

		}
	}

}
